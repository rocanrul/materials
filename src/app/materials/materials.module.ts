import { NgModule } from '@angular/core';
import { MatFormFieldModule } from "@angular/material/form-field"; /* campos de formulario */
import { MatInputModule } from "@angular/material/input"; /* inputs */
import {MatButtonModule} from "@angular/material/button"; /* boton */
import {MatButtonToggleModule} from "@angular/material/button-toggle"; /* boton toggle */
import {MatIconModule} from '@angular/material/icon'; /* iconos */
import {MatBadgeModule} from '@angular/material/badge'; /* notificaciones */
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; /* progreso */
import {MatToolbarModule} from '@angular/material/toolbar'; /* navbar */
import {MatSidenavModule} from '@angular/material/sidenav';  /* side navbar */ //FIXME: VER COMO IMPLEMENTARLO DE MANERA NATURAL
import {MatMenuModule} from '@angular/material/menu'; /* menu desplegable  y menu desplegable hijo*/



const Material  = [
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatBadgeModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule

];

@NgModule({

  imports: [Material],
  exports:[Material]
})
export class MaterialsModule { }

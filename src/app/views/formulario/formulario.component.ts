import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Telefono } from 'src/app/interfaces/formulario/telefono';
import { NotificacionesService } from 'src/app/services/notificaciones.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public inputControl: FormControl;
  form: FormGroup = new FormGroup({
    tel: new FormControl(new Telefono('', '', ''))
  });

  notify: number = 0;
  subscription: Subscription;
  uploading = false;
  gen: string = "Seleccione su genero";



  constructor(private notificacion: NotificacionesService) { }

  ngOnInit(): void {

    this.inputControl = new FormControl();

    this.subscription = this.notificacion.notifyObservable$.subscribe(notify => {
      this.notify = notify;
    });
  }
  /* TODO: */
  newNotification() {
    this.notificacion.onDataChange(this.notify + 1);
  }

  uploadData() {

    this.uploading = true;
    setTimeout(() => {
      this.uploading = false;
    }, 5000)
  }


  genero(gen: number) {
    if (gen == 1) {
   
      this.gen = "Masculino";

    } else if (gen == 2) {
      this.gen = "Femenino";
    }

  }

}


import { Component, OnInit } from '@angular/core';
import { NotificacionesService } from 'src/app/services/notificaciones.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  notify:number;
  subscription: Subscription;
  constructor(private notificacion: NotificacionesService) { }

  ngOnInit(): void {
    this.subscription = this.notificacion.notifyObservable$.subscribe(notify => this.notify = notify);
  }

}

import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  private notify = new Subject<number>();
  notifyObservable$ = this.notify.asObservable();

  
  constructor() { }


  onDataChange(dataSource:number){

    this.notify.next(dataSource);
 
  }

  
}
